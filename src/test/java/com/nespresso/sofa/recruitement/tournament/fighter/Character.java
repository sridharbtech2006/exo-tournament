package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;
import java.util.List;

public abstract class Character {
	
	private int hitpoints;
	
	private int blowPoints;
	
	private List<String> equipment=new ArrayList<String>();
	
	private int defaultHitPoints;
	
	
	
	
	public void setHitpoints(int hitpoints) {
		this.hitpoints = hitpoints;
	}
	
	
	
	public List<String> getEquipment() {
		return equipment;
	}



	public Character equip(String equipment) {
		this.equipment.add(equipment);
		return this;
	}



	public int hitPoints(){
		return hitpoints;
	}
	
	public void removeHitPoints(int points){
		hitpoints=hitpoints-points;
		if(hitpoints<0)
			hitpoints=0;
	}
	
	public abstract void blow(Character character);



	public int getBlowPoints() {
		return blowPoints;
	}



	public void setBlowPoints(int blowPoints) {
		this.blowPoints = blowPoints;
	}
	
	public void recover(int blowpoints){
		setHitpoints(hitPoints()+blowpoints);
	}



	public int getDefaultHitPoints() {
		return defaultHitPoints;
	}



	public void setDefaultHitPoints(int defaultHitPoints) {
		this.defaultHitPoints = defaultHitPoints;
	}

}
