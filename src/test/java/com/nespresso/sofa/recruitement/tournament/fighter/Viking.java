package com.nespresso.sofa.recruitement.tournament.fighter;

public class Viking extends Character {
	
	private final int DEFAULT_POINTS=120;
	
	private final int BLOW_POINT=5;
	
	public Viking(){
	    setHitpoints(120);
	    equip("axe");
	    setBlowPoints(BLOW_POINT);
	    setDefaultHitPoints(DEFAULT_POINTS);
	}
	
	public void recover(){
		setHitpoints(hitPoints()+6);
	}

	@Override
	public void blow(Character character) {
		character.removeHitPoints(6);		
	}
	
	@Override
	public Viking equip(String equipment){
		return (Viking)super.equip(equipment);
	}
	

}
