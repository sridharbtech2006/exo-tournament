package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.configuration.Constants;

public class Swordsman extends Character {
	
	private final int DEFAULT_POINTS=100;
	
	private final int BLOW_POINT=5;
	
	public Swordsman(){
	    setHitpoints(DEFAULT_POINTS);
	    setDefaultHitPoints(DEFAULT_POINTS);
	    equip("sword");
	    setBlowPoints(BLOW_POINT);
	}
	
	
	
		
	public void engage(Character oponent){
		
		System.out.println("*********************************************************");
		int numberOfBlows=0;
		while(this.hitPoints()>0 && oponent.hitPoints()>0){
			
			if(numberOfBlows>0 && (numberOfBlows%2==0)){
				if(this.getEquipment().contains(Constants.BUCKLER) && oponent.getEquipment().contains("axe")  && numberOfBlows<=3)
					recover(oponent.getBlowPoints());
				if(oponent.getEquipment().contains(Constants.BUCKLER))
					oponent.recover(BLOW_POINT);
			}else{
				this.blow(oponent);
				oponent.blow(this);
			}
			numberOfBlows++;	
			
			System.out.println("this.hitPoints() : "+this.hitPoints());
			System.out.println("oponent.hitPoints() : "+oponent.hitPoints());
		}
	}


	@Override
	public void blow(Character character) {
		character.removeHitPoints(5);		
	}
	
	@Override
	public Swordsman equip(String equipment){
		return (Swordsman)super.equip(equipment);
	}
	
	

}
